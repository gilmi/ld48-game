{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}

module Play.Engine.Input where

import Flat.Class
import Data.Word (Word8)
import Data.Tuple
import Data.Maybe
import qualified SDL
import Play.Engine.Types
import qualified Data.Map.Strict as M
import GHC.Generics
import Control.DeepSeq

--import Debug.Trace

data Input
  = Input
    { keyUp    :: !Bool
    , keyDown  :: !Bool
    , keyLeft  :: !Bool
    , keyRight :: !Bool
    , keyA     :: !Bool
    , keyB     :: !Bool
    , keyC     :: !Bool
    , keyD     :: !Bool
    , keyM     :: !Bool
    , keyP     :: !Bool
    , keyScale :: !Bool
    , keyStart :: !Bool
    , keyQuit  :: !Bool
    }
  deriving (Show, Eq, Ord, Generic, NFData, Flat)

type Inputs
  = M.Map PlayerId Keys

type Keys = M.Map Key Action

data Action
  = Idle
  | Release
  | Hold
  | Click
  deriving (Show, Eq, Ord, Generic, NFData)

data Key
  = KeyUp
  | KeyDown
  | KeyLeft
  | KeyRight
  | KeyA
  | KeyB
  | KeyC
  | KeyD
  | KeyM
  | KeyP
  | KeyScale
  | KeyStart
  | KeyQuit
  deriving (Show, Read, Eq, Ord, Bounded, Enum, Generic, NFData, Flat)

emptyInput :: Input
emptyInput = Input
  { keyUp    = False
  , keyDown  = False
  , keyLeft  = False
  , keyRight = False
  , keyA     = False
  , keyB     = False
  , keyC     = False
  , keyD     = False
  , keyM     = False
  , keyP     = False
  , keyScale = False
  , keyStart = False
  , keyQuit  = False
  }

keysToInput :: M.Map Key Bool -> Input
keysToInput mapping = Input
  { keyUp    = fromMaybe False $ M.lookup KeyUp mapping
  , keyDown  = fromMaybe False $ M.lookup KeyDown mapping
  , keyLeft  = fromMaybe False $ M.lookup KeyLeft mapping
  , keyRight = fromMaybe False $ M.lookup KeyRight mapping
  , keyA     = fromMaybe False $ M.lookup KeyA mapping
  , keyB     = fromMaybe False $ M.lookup KeyB mapping
  , keyC     = fromMaybe False $ M.lookup KeyC mapping
  , keyD     = fromMaybe False $ M.lookup KeyD mapping
  , keyM     = fromMaybe False $ M.lookup KeyM mapping
  , keyP     = fromMaybe False $ M.lookup KeyP mapping
  , keyScale = fromMaybe False $ M.lookup KeyScale mapping
  , keyStart = fromMaybe False $ M.lookup KeyStart mapping
  , keyQuit  = fromMaybe False $ M.lookup KeyQuit mapping
  }

inputToKeys :: Input -> M.Map Key Bool
inputToKeys Input{..} = M.fromList
  [ ( KeyUp    , keyUp )
  , ( KeyDown  , keyDown )
  , ( KeyLeft  , keyLeft )
  , ( KeyRight , keyRight )
  , ( KeyA     , keyA )
  , ( KeyB     , keyB )
  , ( KeyC     , keyC )
  , ( KeyD     , keyD )
  , ( KeyM     , keyM )
  , ( KeyP     , keyP )
  , ( KeyScale , keyScale )
  , ( KeyStart , keyStart )
  , ( KeyQuit  , keyQuit )
  ]

empty :: Inputs
empty = mempty

initKeyStats :: Keys
initKeyStats = M.fromList $ zip [minBound..maxBound] (cycle [Idle])

defKeyMap :: [(Key, SDL.Scancode)]
defKeyMap = map swap
  [ (SDL.ScancodeW, KeyUp)
  , (SDL.ScancodeS, KeyDown)
  , (SDL.ScancodeA, KeyLeft)
  , (SDL.ScancodeD, KeyRight)
  , (SDL.ScancodeUp, KeyUp)
  , (SDL.ScancodeDown, KeyDown)
  , (SDL.ScancodeLeft, KeyLeft)
  , (SDL.ScancodeRight, KeyRight)
  , (SDL.ScancodeReturn, KeyStart)
  , (SDL.ScancodeEscape, KeyQuit)
  , (SDL.ScancodeQ, KeyQuit)
  , (SDL.ScancodeZ, KeyA)
  , (SDL.ScancodeX, KeyB)
  , (SDL.ScancodeR, KeyC)
  , (SDL.ScancodeV, KeyD)
  , (SDL.ScancodeM, KeyM)
  , (SDL.ScancodeP, KeyP)
  , (SDL.ScancodeF12, KeyScale)
  ]

-- can't have more than one binding to the same key as this will create a state accumulation problem
defControllerButtonMap :: [(Key, Word8)]
defControllerButtonMap = map swap
  [ (13, KeyUp)
  , (14, KeyDown)
  , (11, KeyLeft)
  , (12, KeyRight)
  , (0, KeyB)
  , (5, KeyA)
  , (3, KeyC)
  , (7, KeyStart)
  ]

keepState :: Maybe Action -> Bool
keepState state
  | state == Just Click = True
  | state == Just Hold = True
  | state == Just Release = False
  | state == Just Idle = False

  | otherwise = False

checkControllerEvent :: Word8 -> SDL.EventPayload -> Maybe Bool
checkControllerEvent btn = \case
  SDL.JoyButtonEvent (SDL.JoyButtonEventData _ btn' SDL.JoyButtonPressed)
    | btn == btn' -> pure True
  SDL.JoyButtonEvent (SDL.JoyButtonEventData _ btn' SDL.JoyButtonReleased)
    | btn == btn' -> pure False
  SDL.JoyAxisEvent (SDL.JoyAxisEventData _ btn' _)
    | btn == btn' -> pure True
  _ -> Nothing

makeEvents :: M.Map Key Bool -> Keys -> Keys
makeEvents isKeyPressed !current =
    updateKeys current isKeyPressed

makeKeyStatus :: (SDL.Scancode -> Bool) -> [(Key, SDL.Scancode)] -> M.Map Key Bool
makeKeyStatus isKeyPressed keyboardKeys =
  let
    keyboard = fmap (fmap isKeyPressed)
    result = M.fromListWith max (keyboard keyboardKeys)
  in
    deepseq result result

updateKeys :: Keys -> M.Map Key Bool -> Keys
updateKeys !keys !newStates =
  flip M.mapWithKey keys $ \k s ->
    case (s, testKey k newStates) of
      (Idle, True) -> Click
      (Click, True) -> Hold
      (Hold, True) -> Hold
      (Release, True) -> Click
      (Idle, False) -> Idle
      (Click, False) -> Release
      (Hold, False) -> Release
      (Release, False) -> Idle


testKey :: Key -> M.Map Key Bool -> Bool
testKey key = maybe False id . M.lookup key

keyReleased :: Key -> Keys -> Bool
keyReleased key = maybe False (== Release) . M.lookup key

keyClicked :: Key -> Keys -> Bool
keyClicked key = maybe False (== Click) . M.lookup key

keyPressed :: Key -> Keys -> Bool
keyPressed key = maybe False (/= Idle) . M.lookup key

keyIdle :: Key -> Keys -> Bool
keyIdle key = maybe False (== Idle) . M.lookup key

keysToMovement :: Float -> Keys -> FPoint
keysToMovement speed keys =
  let
      singleMove k1 k2
        | keyPressed k1 keys && not (keyPressed k2 keys) = -speed
        | keyPressed k2 keys && not (keyPressed k1 keys) =  speed
        | otherwise = 0
      hori = singleMove KeyUp KeyDown
      vert = singleMove KeyLeft KeyRight
  in Point vert hori
