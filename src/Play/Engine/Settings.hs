{-# LANGUAGE TemplateHaskell #-}

module Play.Engine.Settings where

import qualified SDL
import qualified Control.Monad.State as SM
import qualified Data.Map.Strict as M
import Data.Tuple

import Play.Engine.Types
import Play.Engine.Input
import Control.Lens

data Settings
  = Settings
  { _windowSize :: !Size
  , _keyMap :: ![(Key, SDL.Scancode)]
  , _keyStats :: M.Map PlayerId Keys
  , _windowTitle :: String
  , _myPlayerId :: PlayerId
  , _quit :: Bool
  }
  deriving (Show)

makeLenses ''Settings

type Result a = SM.State Settings a

runResult :: s -> SM.State s a -> (s, a)
runResult settings m = swap $ SM.runState m settings

def :: Settings
def = Settings
  { _windowSize = Point 800 600
  , _keyMap = defKeyMap
  , _keyStats = mempty
  , _windowTitle = "Play mi"
  , _myPlayerId = PlayerId 0
  , _quit = False
  }
