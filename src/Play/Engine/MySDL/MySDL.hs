-- MySDL: some wrappers and utility functions around SDL

{-# LANGUAGE LambdaCase, TypeFamilies #-}

module Play.Engine.MySDL.MySDL where

import Data.Word (Word8, Word32)
import Data.Text (Text)
import Control.Monad.Identity
import Control.Monad.IO.Class (MonadIO)
import Control.Concurrent (threadDelay)
import Control.Concurrent.STM
import Control.Concurrent.Async
import qualified Data.Vector as V

import qualified Foreign.C.Types as C
import qualified Data.Map.Strict as M

import qualified SDL
import qualified SDL.Image as SDLI
import qualified SDL.Font as SDLF
import qualified SDL.Mixer as Mix
import SDL.Vect (V2(..), V4(..))

import Play.Engine.Input
import Play.Engine.Settings
import Play.Engine.MySDL.Assets
import Play.Engine.Types

--import Debug.Trace

-- | Config window
myWindowConfig :: V2 C.CInt -> SDL.WindowConfig
myWindowConfig size = SDL.defaultWindow { SDL.windowInitialSize = size }

-- | Init SDL and create a Window and pass in as a parameter to function
withWindow :: Text -> SDL.WindowConfig -> (SDL.Window -> IO a) -> IO a
withWindow title winConf go = do
  SDL.initializeAll
  SDLI.initialize [minBound..maxBound]
  SDLF.initialize

  window <- SDL.createWindow title winConf

  SDL.showWindow window

  mJoystick <- getJoystick

  result <- Mix.withAudio Mix.defaultAudio 256 $ do

    go window

  sequence_ $ SDL.closeJoystick <$> mJoystick
  SDL.destroyWindow window
  SDLI.quit
  SDLF.quit
  SDL.quit

  pure result

withRenderer :: MonadIO m => SDL.Window -> ((SDL.Window, SDL.Renderer) -> m a) -> m a
withRenderer window go = do
  renderer <- SDL.createRenderer window (-1)
    $ SDL.RendererConfig
      { rendererType          = SDL.AcceleratedVSyncRenderer
      , rendererTargetTexture = False
      }
  go (window, renderer)


-- | App loop: takes the current world and functions that updates the world renders it
-- manage ticks, events and loop
apploop
  :: ResourcesT TVar
  -> SDL.Window
  -> SDL.Renderer
  -> (Settings -> a -> IO ())
  -> (M.Map Key Bool -> IO ())
  -> IO ([Request], (Settings, a))
  -> PlayerId
  -> IO ()
apploop resources window renderer render setInput sample pid = do
  -- measure ticks at the start
  start <- SDL.ticks
  events <- collectEvents
  --unless (null events) $ print events
  keyState <- SDL.getKeyboardState
  (reqs, (settings, newWorld)) <- sample
  let input = makeKeyStatus keyState (_keyMap settings)
  setInput $! input
  render settings newWorld
  void $ async $ mapConcurrently_ (\req -> runRequest req resources window renderer) reqs
  if checkEvent SDL.QuitEvent events || _quit settings
  -- then setInput (Input mempty (Just Bye))
  then pure ()
  else do
    when (isWindowHidden events) $ do
      isPlaying <- Mix.playingMusic
      when isPlaying Mix.pauseMusic
      let
        loop evs
          | isWindowExposed evs = when isPlaying Mix.resumeMusic
          | otherwise = loop =<< collectEvents
      loop events

    -- measure ticks at the end and regulate FPS
    end <- SDL.ticks
    -- regulateFPS 60 start end
    apploop resources window renderer render setInput sample pid

-- | Will wait until ticks pass
regulateFPS :: Word32 -> Word32 -> Word32 -> IO ()
regulateFPS fps start end
  | fps == 0 = pure ()
  | otherwise = do
    let
      ticksPerFrame = 1000 `div` fps
      interval = end - start
      gap = ticksPerFrame - interval
      delayFor
        | gap < ticksPerFrame =
          fromIntegral $ max 0 gap
        | otherwise =
          fromIntegral ticksPerFrame
    threadDelay $ delayFor * 1000 -- threadDelay works in microseconds


getJoystick :: IO (Maybe SDL.Joystick)
getJoystick = do
  joysticks <- SDL.availableJoysticks
  let
    joystick =
      if V.length joysticks == 0
        then Nothing
        else pure (joysticks V.! 0)

  sequence $ SDL.openJoystick <$> joystick



setBGColor :: MonadIO m => V4 Word8 -> SDL.Renderer -> m SDL.Renderer
setBGColor color renderer = do
  SDL.rendererDrawColor renderer SDL.$= color
  SDL.clear renderer
  pure renderer

-- | Collect all events from inputs
collectEvents :: MonadIO m => m [SDL.EventPayload]
collectEvents = SDL.pollEvent >>= \case
    Nothing -> pure []
    Just e  -> (SDL.eventPayload e :) <$> collectEvents

-- | Checks if specific event happend
checkEvent :: SDL.EventPayload -> [SDL.EventPayload] -> Bool
checkEvent = elem

isWindowHidden :: [SDL.EventPayload] -> Bool
isWindowHidden = any $ \case
  SDL.WindowHiddenEvent{} -> True
  _ -> False

isWindowExposed :: [SDL.EventPayload] -> Bool
isWindowExposed = any $ \case
  SDL.WindowExposedEvent{} -> True
  _ -> False

