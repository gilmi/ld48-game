{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE TemplateHaskell #-}

module GameScreen where

import qualified SDL
import qualified SDL.Font as SDLF
import qualified SDL.Mixer as Mix
import qualified Play.Engine.MySDL.Assets as Assets

import Play.Engine
import Control.Monad
import Control.Lens
import Data.Maybe
import System.Random (randomRIO)

import qualified Control.Monad.State as SM
import qualified Data.DList as DL
import qualified Data.Map.Strict as M
import qualified Data.Set as S
import qualified Data.ByteString as BS

import qualified Play.Engine.Components.Bullet as B
import qualified Play.Engine.Components.Terrain as Tr
import qualified Play.Engine.ListZipper as Z
import qualified Play.Engine.Script as Script

import qualified Play.Engine.Components.TextBox as TB
import qualified Play.Engine.Sprite as Spr
import qualified Character as C
import qualified End as E


data State
  = State
  { _bga :: Spr.Sprite
  , _chars :: M.Map PlayerId C.Character
  , _bullets :: DL.DList B.Bullet
  , _terrains :: Z.ListZipper V2 Tr.Terrain
  , _resources :: Assets.Resources
  , _cameraShake :: Int
  , _cameraY :: Int
  , _isPause :: !Bool
  , _pauseChanged :: !Bool
  , _hudFont :: SDLF.Font
  , _exit :: !Bool
  , _script :: Script.Script ()
  }

makeFieldsNoPrefix ''State

wantedAssets :: [(String, Assets.ResourceType FilePath)]
wantedAssets =
  [ ("gamebg", Assets.Texture "gamebg.png")
  , ("unispace", Assets.Font "unispace/unispace.ttf")
  , ("music", Assets.Music "deep.mp3")
  --, ("bga", MySDL.Texture "bga.png")
  ] <> C.wantedAssets
    <> E.wantedAssets

mkState
  :: Assets.Resources -> Scene
mkState rs =
  let state = initState rs
  in mkScene
    state
    update
    render

initState :: Assets.Resources -> State
initState rs = do
  case (,,,)
    <$> M.lookup "gamebg" (Assets.textures rs)
    <*> M.lookup "unispace" (Assets.fonts rs)
    <*> M.lookup "music" (Assets.music rs)
    <*> M.lookup "music-end" (Assets.music rs)
    of
    Nothing ->
      error "Texture or font not found: bg / unispace / end-music"
    Just (bgt, font, music, endMusic) ->
      -- mc' <- (SB.mkMainChar $ MySDL.textures rs)
      State
        { _bga =
          maybe undefined id $ Spr.make $ Spr.MakeArgs
            { mkActionmap = ["normal"]
            , mkAction = "normal"
            , mkTexture = bgt
            , mkSize = Point 768 4500
            , mkMaxPos = 4
            , mkSpeed = 2
            }
        , _chars = mempty
        , _bullets = mempty
        , _terrains = Z.ListZipper
          []
          ( V2
            (Tr.mkTerrain (mkColor 205 0 50 100) (V2 800 600) (V2 (-10) 400))
            (Tr.mkTerrain (mkColor 205 0 50 100) (V2 800 600) (V2 (-10) 1000))
          )
          ( map (\i -> Tr.mkTerrain (mkColor 205  0 50 100) (V2 800 600) (V2 (-10) (1000 + 600 * i))) [1..5]
          )
        , _resources = rs
        , _cameraShake = 0
        , _cameraY = 0
        , _isPause = False
        , _pauseChanged = False
        , _hudFont = font
        , _exit = False
        , _script = gamescript font music endMusic
        }

gamescript :: SDLF.Font -> BS.ByteString -> BS.ByteString -> Script.Script ()
gamescript font music endMusic =
  [ Script.LoadTextBox Script.act{ Script.stopTheWorld = True } $
    TB.make TB.Bottom 5
    "Dig deeper! (Use: Arrows / Z / X)"
    Nothing font
  , Script.PlayMusic Mix.Forever ("music", Just music)
--  , Script.WaitUntil Script.noAction (\pc _ -> fmap (\(V2 _ y_) -> y_) pc >= Just (200 + 600 * 0))
  , Script.WaitUntil Script.noAction (\pc _ -> fmap (\(V2 _ y_) -> y_) pc >= Just (1000 + 576 * 5))
  , Script.FadeOut Script.act { Script.command = Replace $ E.end True font endMusic } 0
  , Script.StopMusic
  ]

initEnemyTimer :: Int
initEnemyTimer = 60

update :: Inputs -> Settings -> State -> Result (StackCommand, State)
update input settings state = do
  wSize <- _windowSize <$> SM.get
--  ismute <- _muteMusic <$> SM.get

  let
    txts = Assets.textures (state ^. resources)
    addChars cs =
      let
        players = M.keysSet cs
        connectedPlayers = M.keysSet input
        newPlayers = connectedPlayers S.\\ players
        findDisco :: PlayerId -> (PlayerId, C.Character)
        findDisco pid =
          maybe (pid, C.mkCharacter pid txts) (\c -> (c ^. C.playerId, set C.playerId pid c))
            . listToMaybe
            . map snd
            $ M.toList (M.filter (not . C._connected) cs)
      in
        foldr
          ( \pid acc ->
            let
              (oldPid, char) = findDisco pid
            in
              M.insert pid char $ M.delete oldPid acc
          )
        cs
        newPlayers

    state''' = state
      & over chars addChars

  (M.fromList -> chars', DL.concat -> newBullets) <- do
    unzip <$> mapM
      ( \(pid, char) -> do
        (char', bulllets) <-
          C.update
          input -- (maybe input (dirToInput . dirToPlace (state ^. mc . pos)) (Script.moveMC acts))
          (state ^. cameraY)
          (\c -> fmap (flip Tr.getCollisions c) (Z.getF $ state ^. terrains))
          char
        pure ((pid, char'), bulllets)
      )
      (M.toList $ state''' ^. chars)

  let
    (bullets', _enemiesHit) =
      updateListWith
        M.empty
        (const $ const M.empty)
        (B.update (state ^. cameraY) wSize isTouchingCircleRect (M.elems $ state ^. chars))
        (state ^. bullets)

    bullets'' =
      Tr.updateCollided (Z.getF $ state ^. terrains) bullets'

    state'' = state'''
      & set chars chars'
      & set bullets (DL.append newBullets bullets'')
      & over terrains (Z.overCurr $ Tr.update (M.elems chars'))
      & over terrains (Z.overCurr $ Tr.update (DL.toList bullets'))
      & over cameraY
        ( max 0
        . maybe id (changeCamera (settings ^. windowSize . y))
          (M.lookup (settings ^. myPlayerId) (state ^. chars))
        )

  (acts, script') <- Script.update input (Just (V2 0 (state'' ^. cameraY))) [] (state'' ^. script)

  let
    state' = state''
      & over terrains
        ( \z@(Z.ListZipper prev (V2 curr1 curr2) next) ->
          case
            ( (state'' ^. cameraY) <= curr1 ^. pos . y
            , prev
            , (state'' ^. cameraY) + (settings ^. windowSize . y) >= (curr2 ^. pos . y) + (curr2 ^. size . y)
            , next
            ) of
            (True, p : rest, _, _) ->
              Z.ListZipper rest (V2 p curr1) (curr2 : next)
            (_, _, True, n : rest) ->
              Z.ListZipper (curr1 : prev) (V2 curr2 n) rest
            _ -> z
        )
      & set script script'

  if
    | any (keyReleased KeyP) input && state ^. isPause -> do
      pure (None, set pauseChanged True $ set isPause False state)
    | any (keyReleased KeyP) input && not (state ^. isPause) -> do
      pure (None, set pauseChanged True $ set isPause True state)
    | state ^. isPause -> do
      pure (None, state)
    | any (keyReleased KeyQuit) input -> do
      pure (None, set exit True state)
    | state ^. exit -> do
      pure (Done, state)
    | otherwise -> do
      pure (Script.command acts, state')

changeCamera :: Int -> C.Character -> Int -> Int
changeCamera winHeight char camY
  | char ^. pos . y < camY + (winHeight `div` 4) =
    camY - 1

  | char ^. pos . y > camY + (2 * (winHeight `div` 4)) =
    camY + 1

  | otherwise = camY

flipEnemyDir :: Either () () -> Either () ()
flipEnemyDir = \case
  Right () -> Left ()
  Left () -> Right ()

render :: SDL.Renderer -> Settings -> State -> IO ()
render renderer settings state = do
  SDL.clear renderer
  cam' <- Point <$> randomRIO (-1, 1) <*> randomRIO (-1, 1) :: IO FPoint
  let
    cam =
      addPoint (Point 0 (0 - fromIntegral (state ^. cameraY))) .
        addPoint (fmap (floor . (*) (fromIntegral $ state ^. cameraShake `div` 3)) cam')
  Spr.render renderer
    cam
    (Point 0 0) (state ^. bga . size) 255 (state ^. bga)
  mapM_ (C.render renderer cam) (state ^. chars)
  mapM_ (B.render renderer cam) (state ^. bullets)
  mapM_ (Tr.render renderer cam) (Z.getF $ state ^. terrains)

  Script.render renderer (settings ^. windowSize) cam (state ^. script)
--  when (state ^. isMute) $
--    renderText renderer (state ^. hudFont) (Point 40 30) "MUTED"

  when (state ^. isPause)
    $ renderText renderer (state ^. hudFont) (Point 370 380) "PAUSE"

  when (state ^. pauseChanged) $
    if (state ^. isPause)
      then
        Mix.pauseMusic
      else
        Mix.resumeMusic

  when (state ^. exit) $
    Mix.haltMusic
