{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeSynonymInstances  #-}
{-# LANGUAGE FlexibleInstances  #-}

module Play.Engine.Components.Terrain where

import qualified SDL

import Data.Word
import Play.Engine
import Control.Lens
import Control.DeepSeq
import qualified Data.Vector.Storable as V
import Data.Vector.Storable.ByteString
import qualified Data.ByteString as BS
import qualified Data.DList as DL
import qualified Play.Engine.Components.Bullet as Bullet

import Debug.Trace

data Terrain
  = Terrain
  { _pos :: {-# UNPACK #-} !IPoint
  , _size :: {-# UNPACK #-} !Size
  , _array :: V.Vector Word32
  }

makeFieldsNoPrefix ''Terrain

instance NFData Terrain where
  rnf (Terrain {_pos, _size}) =
    rnf _pos
    `seq` rnf _size

mkTerrain :: Word32 -> IPoint -> IPoint -> Terrain
mkTerrain txt sz position = Terrain
  { _pos = position
  , _size = sz
  , _array = V.replicate (sz ^. x * sz ^. y) txt
  }

update entities terrain
--  | not $ uncurry isInWindow cam (terrain ^. pos) (terrain ^. size)
--  = terrain
  | otherwise =
    let
      collisions = V.concat $ fmap (getCollisions terrain) entities
      colors = V.replicate (V.length collisions) (mkColor 0 0 0 0)
    in
      terrain
        { _array = V.update_ (terrain ^. array) collisions colors
        }

getCollisions terrain e =
  -- fmap (getOverlappingIndices terrain) entities
  let
    indices_ = (getOverlappingIndices terrain e)
  in
    V.filter (\i -> (mkColor 0 0 0 0) /= (terrain ^. array) V.! i) indices_

updateCollided terrains entities =
  DL.map
    (\b -> over
       Bullet.bullet_dmg
       ( subtract $ sum $ fmap V.length $ fmap (flip getCollisions b) terrains
       )
       b
    )
    entities

getOverlappingIndices
  :: (HasSize e1 Size, HasPos e1 IPoint)
  => (HasPos e2 IPoint, HasHitbox e2 Hitbox)
  => e1 -> e2 -> V.Vector Int
getOverlappingIndices terrain e =
  let
    tx = terrain ^. pos . x
    ty = terrain ^. pos . y
    tw = terrain ^. size . x
    th = terrain ^. size . y
    startx = e ^. pos . x + e ^. hitbox . alignment . x - tx
    starty = e ^. pos . y + e ^. hitbox . alignment . y - ty
    ew = e ^. hitbox . size . x
    eh = e ^. hitbox . size . y
    start = startx + starty * tw
    bounds i
      | 0 <= i && i < (th * tw) = i
      | otherwise = -1
  in
    V.filter (0<) $ V.generate
      (ew * eh)
      (\i -> bounds $ start + (i `div` ew * tw) + (i `mod` ew))


render :: SDL.Renderer -> Camera -> Terrain -> IO ()
render renderer cam terrain = do
  txt <- SDL.createTexture renderer SDL.RGBA8888 SDL.TextureAccessStatic (fmap fromIntegral $ terrain ^. size)
  let dat = vectorToByteString (terrain ^. array)
  _ <- SDL.updateTexture txt Nothing dat (fromIntegral (terrain ^. size . x * 4))
  SDL.textureBlendMode txt SDL.$= SDL.BlendAlphaBlend
  SDL.copy renderer txt Nothing (Just $ toRect (cam $ terrain ^. pos) (terrain ^. size))
  SDL.destroyTexture txt
