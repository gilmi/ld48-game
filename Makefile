.PHONY: setup

setup:
	stack setup

.PHONY: build

build:
	stack build --ghc-options='-Werror'

.PHONY: dev

dev:
	stack build --fast --file-watch

.PHONY: exec

exec:
	stack build && stack exec deeper

.PHONY: run

run:
	stack build && stack exec deeper -- +RTS -sstderr

.PHONY: profile

profile:
	stack build --profile && stack exec --profile deeper -- +RTS -sstderr -p -hc && stack exec -- hp2ps -c deeper.hp && evince deeper.ps

.PHONY: profile-network

profile-network:
	stack build --profile && stack exec --profile deeper -- 127.0.0.1 +RTS -sstderr -p -hc && stack exec -- hp2ps -c deeper.hp && evince deeper.ps

.PHONY: clean

clean:
	stack clean

