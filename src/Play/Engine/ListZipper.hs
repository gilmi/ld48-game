{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveFoldable, DeriveTraversable #-}

module Play.Engine.ListZipper where

import GHC.Generics
import Control.DeepSeq
import Control.Monad.Identity

data ListZipper f a
  = ListZipper [a] !(f a) [a]
  deriving (Eq, Ord, Show, Read, Functor, Foldable, Generic, NFData)

instance Traversable t => Traversable (ListZipper t) where
  traverse f (ListZipper p c n) =
    ListZipper
      <$> fmap reverse (traverse f (reverse p))
      <*> traverse f c
      <*> traverse f n

getF :: ListZipper f a -> f a
getF (ListZipper _ x _) = x

get :: ListZipper Identity a -> a
get (ListZipper _ (Identity x) _) = x

overCurr :: Functor f => (a -> a) -> ListZipper f a -> ListZipper f a
overCurr f (ListZipper p x n) = ListZipper p (fmap f x) n

nextStop :: ListZipper Identity a -> ListZipper Identity a
nextStop = \case
  ListZipper prev (Identity curr) (n:next) ->
    ListZipper (curr : prev) (Identity n) next
  l -> l

nextCycle :: ListZipper Identity a -> ListZipper Identity a
nextCycle = \case
  ListZipper prev (Identity curr) (n:next) ->
    ListZipper (curr : prev) (Identity n) next
  ListZipper prev (Identity curr) [] ->
    let
      (curr' : next) = reverse $ curr : prev
    in
      ListZipper [] (Identity curr') next


prevCycle :: ListZipper Identity a -> ListZipper Identity a
prevCycle = \case
  ListZipper (p:prev) (Identity curr) next ->
    ListZipper prev (Identity p) (curr : next)
  ListZipper [] (Identity curr) next ->
    let
      (curr' : prev) = reverse $ curr : next
    in
      ListZipper prev (Identity curr') []

first :: ListZipper Identity a -> a
first = \case
  ListZipper [] (Identity curr) _ ->
    curr
  ListZipper prev _ _ ->
    Prelude.last prev

last :: ListZipper Identity a -> a
last = \case
  ListZipper _ (Identity curr) [] ->
    curr
  ListZipper _ _ next ->
    Prelude.last next

diffMapM :: Applicative f => (a -> f b) -> (a -> f b) -> ListZipper Identity a -> f (ListZipper Identity b)
diffMapM restF currF (ListZipper prev curr next) =
  ListZipper
    <$> traverse restF prev
    <*> traverse currF curr
    <*> traverse restF next

addIndex :: ListZipper Identity a -> ListZipper Identity (Int, a)
addIndex = \case
  ListZipper prev (Identity curr) next ->
    ListZipper
      (reverse $ zip [0..] $ reverse prev)
      (Identity (length prev, curr))
      (zip [length prev + 1..] next)
