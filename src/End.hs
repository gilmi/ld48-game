{-# LANGUAGE OverloadedStrings #-}

module End where

import qualified Play.Engine.MySDL.Assets as Assets
import qualified SDL.Mixer as Mix
import qualified SDL.Font as SDLF

import Data.Maybe (fromJust)
import Play.Engine.Script
import Play.Engine
import qualified Play.Engine.Scenes.AutoScroll as Auto
import qualified Play.Engine.Components.TextBox as TB
import qualified Data.Map as M
import qualified Data.ByteString as BS


end :: Bool -> SDLF.Font -> BS.ByteString -> Scene
end playMusic font endMusic =
  Auto.make (lScript playMusic font endMusic)


wantedAssets :: [(String, Assets.ResourceType FilePath)]
wantedAssets =
  TB.wantedAssets
  ++ [ ("music-end", Assets.Music "deep-end.mp3")
     ]


lScript :: Bool -> SDLF.Font -> BS.ByteString -> Script ()
lScript playMusic font endMusic =
  [ PlayMusic Mix.Once ("music-end", Just endMusic)
  | playMusic
  ] ++
  [ TextUp 2 font (Point 200 600) "You found the bones without giving up!"
  , TextUp 2 font (Point 240 600) "Well done!"
  , Wait noAction 60
  , StopMusic
  , Wait act{ command = Done } 120
  ]
