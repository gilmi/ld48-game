{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE DeriveAnyClass #-}

module Play.Engine.Scene where

import Prelude hiding (head)
import qualified Play.Engine.MySDL.Assets as Assets
import SDL
import Play.Engine.Utils
import Play.Engine.Input
import Play.Engine.Settings
import Control.DeepSeq
import GHC.Generics


-----------
-- Scene --
-----------

-- | Scene describes a generic interface for a game Scene
--   * state: for the scene data
--   * update: how to update the scene
--   * render: how to render the scene
data SceneF a
  = SceneF
  { scState :: a
  , scUpdate
      :: Inputs
      -> Settings
      -> a
      -> Result ([Assets.Request], (StackCommand, a))
  , scRender :: SDL.Renderer -> Settings -> a -> IO ()
  }
  deriving (Generic, NFData)

-- | Existentially Quantified Scene
data Scene
  = forall s. Scene (SceneF s)

-- | Create a Scene from the interface
mkScene
  :: a
  -> (Inputs -> Settings -> a -> Result (StackCommand, a))
  -> (SDL.Renderer -> Settings -> a -> IO ())
  -> Scene
mkScene s u r = Scene (SceneF s (\i st a -> fmap pure $ u i st a) r)

{-
-- a sample definition of a state. Does nothing.
sample :: Scene
sample = mkScene
  ()
  (\_ () -> pure (None, ()))
  (const pure)
-}

-----------------
-- State stack --
-----------------

-- | A command for the scene stack, to be returned by `update`
data StackCommand
  = Done -- ^ You can remove me from the stack
  | None -- ^ Keep me at the top of the stack
  | Push Scene -- ^ Push a new scene
  | Replace Scene -- ^ Replace me with a new scene

-- | Update the top state on the stack
updateScenes :: Inputs -> Settings -> Stack Scene -> Result ([Assets.Request], Stack Scene)
updateScenes input settings scenes = do
  (reqs, (cmd, newState)) <- updateScene input settings (head scenes)
  pure $ (reqs,) $ case cmd of
    Done -> case pop scenes of
      (_, Nothing) -> error "Unexpected empty stack of states"
      (_, Just rest) -> rest
    None -> replace newState scenes
    Replace otherState -> replace otherState scenes
    Push otherState -> push otherState (replace newState scenes)

-- | Update an existentially quantified Scene
updateScene :: Inputs -> Settings -> Scene -> Result ([Assets.Request], (StackCommand, Scene))
updateScene input settings = \case
  Scene s -> do
    (scUpdate s) input settings (scState s) >>= \case
      (reqs, (cmd, newState)) -> pure (reqs, (cmd, Scene $ s { scState = newState }))

-- | Render the top scene on the stack
renderTopScene :: SDL.Renderer -> Settings -> Stack Scene -> IO ()
renderTopScene sdlRenderer settings states = case head states of
  Scene s ->
    (scRender s) sdlRenderer settings (scState s)
