{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}

module Play.Engine.Runner where

import qualified Data.Map.Strict as M
import Control.Monad.IO.Class (MonadIO)
import Control.Monad
import Data.Maybe
import qualified SDL
import SDL.Vect (V2(..), V4(..))
import Control.Lens hiding (sets)
import Control.DeepSeq
import Control.Concurrent.STM.TQueue
import qualified Data.Text as T

import qualified Play.Engine.MySDL.MySDL as MySDL
import qualified Play.Engine.MySDL.Assets as Assets
import Play.Engine.Input
import Play.Engine.Types
import Play.Engine.Utils
import Play.Engine.Settings
import Play.Engine.Scene


-----------
-- Logic --
-----------

mkGame settings = do
  pure
    ( \loader setInput mkClient sampleworld stop getPlayerId -> MySDL.withWindow
      (T.pack $ _windowTitle settings)
      (MySDL.myWindowConfig (V2 (winSize x) (winSize y)))
      ( flip MySDL.withRenderer
        ( \(window, ren) -> do
          resources <- Assets.initResources
          Just assets <- loader resources window ren
          client <- mkClient assets
          -- setInput client (Input mempty (Just Hi))
          MySDL.apploop
            resources
            window
            ren
            (render (window, ren))
            (setInput client . keysToInput)
            (sampleworld client)
            (getPlayerId client)
          stop client
        )
      )
    )
  where
    winSize l = fromIntegral $ settings ^. windowSize . l

update
  :: M.Map PlayerId Input
  -> (Settings, Stack Scene)
  -> ([Assets.Request], (Settings, Stack Scene))
update (fmap inputToKeys -> inputs) (settings, stack) =
  let
    updatePlayer :: PlayerId -> Keys -> Keys
    updatePlayer player pkeys =
      let
        input = fromMaybe mempty $ M.lookup player inputs
      in
        updateKeys pkeys input

    currentInputs =
      (_keyStats settings `M.intersection` inputs) <> fmap (const initKeyStats) inputs
    newInputs :: Inputs
    newInputs =
      M.mapWithKey
        updatePlayer
        currentInputs

    settings' = settings
      & set keyStats newInputs

    toggleMuteCmd
      | otherwise = id

  in
    (\(setts, (reqs, states)) -> (toggleMuteCmd reqs, (setts, states)))
      . (newInputs `deepseq` runResult $! settings')
      $ updateScenes newInputs settings stack

render :: (SDL.Window, SDL.Renderer) -> Settings -> Stack Scene -> IO ()
render (_, renderer) settings stack = do
  SDL.clear renderer
  SDL.rendererDrawBlendMode renderer SDL.$= SDL.BlendAlphaBlend
  renderTopScene renderer settings stack
  SDL.present renderer

setBGColorBlack :: MonadIO m => (SDL.Window, SDL.Renderer) -> m (SDL.Window, SDL.Renderer)
setBGColorBlack sdlStuff@(_, renderer) = do
  void $ MySDL.setBGColor (V4 0 0 0 255) renderer
  pure sdlStuff

