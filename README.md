# deeper

A [LD48 "game"](https://ldjam.com/events/ludum-dare/48/deep-1) I worked on during the jam, made in 72 hours using the [nyx-game](https://gitlab.com/gilmi/nyx-game) codebase.

## Watch on youtube:

Single player:

[![Deep LD48 on youtube](gameplay.png)](http://www.youtube.com/watch?v=N-3mUBAxGs8 "Deep LD48 on youtube")

Online multiplayer:

[![Deep LD48 online multiplayer on youtube](multiplayer.png)](https://www.youtube.com/watch?v=NaioSq4APrk "Deep LD48 online multiplayer on youtube")

## Build From Source

You will need [Stack](https://haskellstack.org).

### Ubuntu:

```sh
sudo apt install libsdl2-dev libsdl2-ttf-dev libsdl2-image-dev libsdl2-mixer-dev
stack build
```

### OS X

```sh
brew install sdl2 sdl2_ttf sdl2_image sdl2_mixer
stack build
```

### Windows

```sh
stack exec -- pacman -Syu
stack exec -- pacman -S mingw-w64-x86_64-pkg-config mingw-w64-x86_64-SDL2 mingw-w64-x86_64-SDL2_image mingw-w64-x86_64-SDL2_ttf mingw-w64-x86_64-SDL2_mixer
stack build
```

## Run

The game has two modes, single player and multi player. Multi player code is using [alpaca-netocode](https://github.com/DavidEichmann/alpaca-netcode).

### Single player

```sh
stack run
```

### Multi player


#### Warning

Use at your own risk! When all clients start together things work, but if one or a few starts playing and another joins later,
there's a real risk that the joining player's computer will crash from trying to evaluate all of the game states too fast!

```sh
stack run -- <hostname> <8111> # e.g. stack run -- 5.180.31.107 8111
```

You will need a server software up an running, you can statically build the one in the `server` directory using docker with `stack build`,
or if you don't have docker and want to build it remove the following lines from the `stack.yaml` file:

```
docker:
  enable: true
  image: utdemir/ghc-musl:v19-ghc8104
```

And remove the following lines from the `netcode-server.cabal` file:

```
      - -static
      - -optl-static
      - -optl-pthread
      - -fPIC
```

And compile and run with `stack run`.


