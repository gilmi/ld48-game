{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module Character where

import qualified SDL
import qualified Play.Engine.MySDL.MySDL as MySDL
import qualified Play.Engine.MySDL.Assets as Assets

import Play.Engine

import Data.Maybe
import Control.Monad.Except
import Control.Lens
import Control.DeepSeq
import qualified Control.Monad.State as SM
import qualified Data.DList as DL
import qualified Data.Map.Strict as M
import qualified Data.Vector.Storable as V

import qualified Play.Engine.Movement as MV
import qualified Play.Engine.Sprite as Spr
import Play.Engine.Components.Bullet

import Debug.Trace

-- import Debug.Trace

data Dir
  = DirRight
  | DirLeft
  deriving (Eq, Show)

data Character
  = Character
  { _playerId :: {-# UNPACK #-} !PlayerId
  , _pos :: {-# UNPACK #-} !IPoint
  , _size :: {-# UNPACK #-} !Size
  , _movement :: {-# UNPACK #-} !MV.Movement
  , _sprite :: !Spr.Sprite
  , _bullet :: !SDL.Texture
  , _hitTimer :: {-# UNPACK #-} !Int
  , _bulletsTimer :: {-# UNPACK #-} !Int
  , _health :: {-# UNPACK #-} !Int
  , _lastDir :: !Dir
  , _hitbox :: !Hitbox
  , _connected :: !Bool
  }

makeFieldsNoPrefix ''Character

instance NFData Character where
  rnf (Character {_playerId, _pos, _size, _hitTimer, _movement, _health, _bulletsTimer, _connected}) =
    rnf _playerId
    `seq` rnf _pos
    `seq` rnf _size
    `seq` rnf _movement
    `seq` rnf _hitTimer
    `seq` rnf _bulletsTimer
    `seq` rnf _health
    `seq` rnf _transparency
    `seq` rnf _connected

instance Eq Character where
  mc1 == mc2 =
    mc1 ^. pos == mc2 ^. pos
    && mc1 ^. size == mc2 ^. size

instance Ord Character where
  mc1 <= mc2 =
    mc1 ^. pos <= mc2 ^. pos
    && mc1 ^. size <= mc2 ^. size

wantedAssets :: [(String, Assets.ResourceType FilePath)]
wantedAssets =
  [ ("nyx-sprites", Assets.Texture "nyx-sprites-animated.png")
  , ("nyx-bullet", Assets.Texture "nyx-bullet.png")
  ]


mkCharacter :: PlayerId -> M.Map String SDL.Texture -> Character
mkCharacter pid ts = do
  case mapM ((`M.lookup` ts) . fst) wantedAssets of
    Just [nyxSprites, nyxBullet] ->
      Character
        { _playerId = pid
        , _pos = Point 100 100
        , _size = charSize
        , _sprite =
          fromJust
            $ Spr.make
            $ Spr.MakeArgs
            { mkActionmap = ["normal", "side-right", "side-left"]
            , mkAction = "normal"
            , mkTexture = nyxSprites
            , mkSize = Point 180 380
            , mkMaxPos = 4
            , mkSpeed = 6
            }
        , _bullet = nyxBullet
        , _hitTimer = -1
        , _bulletsTimer = bulletCooldown
        , _health = 1
        , _movement = MV.make $ MV.defArgs
          { MV.maxspeed = Point 5 5
          , MV.accel = Point 3.5 3.5
          }
        , _lastDir = DirRight
        , _hitbox = fullHitbox
        , _connected = True
        }
    _ ->
      error "Texture not found: nyx-sprites"

bulletCooldown :: Int
bulletCooldown = 5

charSize :: Size
charSize = Point 60 108

fullHitbox = Hitbox
  { _alignment = Point (charSize ^. x `div` 8) (charSize ^. y `div` 8)
  , _size = charSize
    & over x (\s -> s - ((*2) $ s `div` 8))
    & over y (\s -> s - ((*2) $ s `div` 8))
  }

fixHitpos :: Character -> Character
fixHitpos mc = mc
  & set hitbox fullHitbox

halfHitbox :: Character -> Character
halfHitbox mc = mc
  & set hitbox fullHitbox


update :: Foldable t => Inputs -> Int -> (Character -> t (V.Vector Int)) -> Character -> Result (Character, DL.DList Bullet)
update inputs cameraY checkTerrainCollision mc =
  case M.lookup (mc ^. playerId) inputs of
    Nothing -> pure (set connected False mc, mempty)
    Just input -> do
      wsize <- _windowSize <$> SM.get
      let
        dir = keysToMovement 1 input
        (mv, move) =
          MV.update dir
            . set MV.maxSpeed (if keyPressed KeyB input then Point 2.2 2.2 else Point 3 3)
            $ (mc ^. movement)

        newBullets
          | keyPressed KeyA input
          , mc ^. bulletsTimer == 0 =
            DL.fromList (newBullet mc)
          | otherwise = mempty

        newDir
          | keyPressed KeyLeft  input = DirLeft
          | keyPressed KeyRight input = DirRight
          | otherwise = mc ^. lastDir

        newMC =
          mc
          & set connected True
          & over pos (`addPoint` move)
          & fixPos (Point (wsize ^. x) (wsize ^. y + cameraY))
          & fixHitpos
          & set (size . x) (if keyPressed KeyB input then charSize ^. x `div` 2 else charSize ^. x)
          & (if keyPressed KeyB input then halfHitbox else id)
          & set movement mv
          & set lastDir newDir
          & over sprite
            (flip Spr.update False $
               if
                 | keyPressed KeyB input && newDir == DirRight -> Just "side-right"
                 | keyPressed KeyB input -> Just "side-left"
                 | otherwise -> Just "normal"
            )
          & over hitTimer (\t -> if t <= 0 then -1 else t - 1)
          & over bulletsTimer (\t -> if t > 0 then t - 1 else if keyPressed KeyA input then bulletCooldown else 0)

        newMC'
          | any ((0<) . V.length) (checkTerrainCollision newMC) =
            newMC & set pos (mc ^. pos)
          | otherwise =
            newMC

        result =
          if mc ^. health <= 0 && mc ^. hitTimer < 0
            then (set size (Point 0 0) mc, mempty)
            else (newMC', newBullets)

      pure result

newBullet :: Character -> [Bullet]
newBullet mc =
    let
      (loc, dir, mv)
        | Spr.getAction (mc ^. sprite) == Just "side-left" =
          ( addPoint
            (mc ^. hitbox . alignment)
            (Point (-15) (mc ^. hitbox . size . y `div` 2))

          , Point (-1) (0.2 * (MV.direction (mc ^. movement) ^. y))

          , MV.make $ MV.defArgs
            { MV.maxspeed = Point 6 4
            , MV.accel = Point 1 0.2
            }
          )
        | Spr.getAction (mc ^. sprite) == Just "side-right" =
          ( addPoint
            (mc ^. hitbox . alignment)
            (Point (mc ^. hitbox . size . x + 20) (mc ^. hitbox . size . y `div` 2))

          , Point 1 (0.2 * (MV.direction (mc ^. movement) ^. y))

          , MV.make $ MV.defArgs
            { MV.maxspeed = Point 6 4
            , MV.accel = Point 1 0.2
            }
          )
        | otherwise =
          ( addPoint
            (mc ^. hitbox . alignment)
            (Point (mc ^. hitbox . size . x `div` 2) (charSize ^. y))

          , Point (0.2 * (MV.direction (mc ^. movement) ^. x)) ((MV.direction (mc ^. movement) ^. y + 2))

          , MV.make $ MV.defArgs
            { MV.maxspeed = Point 8 6
            , MV.accel = Point 0.2 1
            }
          )

    in
    [ mkBullet
      (mc ^. bullet)
      (fmap (*2) dir) -- (Point 0 1)
      mv
      5 -- damage
      230 -- opacity
--    ((mc ^. pos) `addPoint` Point (charSize ^. x `div` 2) 0)
      ((mc ^. pos) `addPoint` loc)
    ]

checkHit :: [Bullet] -> Character -> Character
checkHit bullets mc
  | not (null bullets) && mc ^. health > 0
  = mc
    & over health (flip (-) (maximum $ (0:) $ map (^. damage) bullets))
    & \mc' -> set hitTimer (if mc' ^. health <= 0 then hitTimeout * 4 else hitTimeout) mc'
  | otherwise
  = mc

hitTimeout = 20

render :: SDL.Renderer -> Camera -> Character -> IO ()
render renderer cam mc =
  unless (mc ^. health < 0 && mc ^. hitTimer < 0) $ do
    let
      isHit = mc ^. hitTimer > 0 && mc ^. hitTimer `mod` 8 < 4
      transp
        | isHit = 130
        | not $ mc ^. connected = 90
        | otherwise = 255
    Spr.render renderer cam (mc ^. pos) charSize transp (mc ^. sprite)

get mc l
  | mc ^. health <= 0 && mc ^. hitTimer < 0 = Nothing
  | otherwise = pure $ mc ^. l
