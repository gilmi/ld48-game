module Play.Engine.Script where

import Data.Word (Word8)
import Data.Maybe
import qualified SDL
import qualified SDL.Font as SDLF
import qualified SDL.Mixer as Mix
import qualified Play.Engine.MySDL.Assets as Assets
import qualified Data.ByteString as BS
import qualified Data.Text as T

import qualified Play.Engine.Components.TextBox as TB
import Control.Lens
import Control.Monad.Except
import Play.Engine
import qualified Play.Engine.Input as I
import qualified Play.Engine.Sprite as Spr


data Command obj
  = Wait !(Actions obj) Int
  | WaitUntil !(Actions obj) (Maybe IPoint -> [obj] -> Bool)
  | If (Maybe IPoint -> [obj] -> Bool) (Script obj)
  | Spawn (Result [obj])
  | LoadTextBox !(Actions obj) (Result TB.TextBox)
  | WaitTextBox !(Actions obj) TB.TextBox
  | PlayMusic Mix.Times (String, Maybe BS.ByteString)
  | PlayMusic' Mix.Times BS.ByteString
  | StopMusic
  | Shake
  | FadeOut !(Actions obj) Word8
  | FadeIn !(Actions obj) Word8
  | TextUp Word8 SDLF.Font IPoint T.Text

data ScriptData obj
  = Script
  { assets :: [(String, Assets.ResourceType FilePath)]
  , script :: Assets.Resources -> Script obj
  , restart :: Scene
  }

type Script obj = [Command obj]

data Actions obj
  = Actions
  { moveMC :: Maybe IPoint
  , spawn :: [obj]
  , stopTheWorld :: Bool
  , shake :: Bool
  , playMusic :: MusicAction
  , command :: StackCommand
  , changeSprite :: Maybe Spr.Sprite
  }

data MusicAction
  = MAPlay (String, FilePath)
  | MAContinue
  | MAStop

noAction :: Actions obj
noAction = Actions
  { moveMC = Nothing
  , spawn = []
  , stopTheWorld = False
  , playMusic = MAContinue
  , shake = False
  , command = None
  , changeSprite = Nothing
  }

act :: Actions obj
act = noAction

update :: I.Inputs -> Maybe IPoint -> [obj] -> Script obj -> Result (Actions obj, Script obj)
update input mcPos objs = \case
  [] -> pure (noAction, [])
  Wait acts i : rest
    | i <= 0 -> pure (acts, rest)
    | otherwise -> pure (acts, Wait acts (i-1) : rest)

  WaitUntil acts test : rest
    | test mcPos objs -> pure (acts, rest)
    | otherwise -> pure (acts, WaitUntil acts test : rest)

  If test cmds : rest
    | test mcPos objs -> pure (noAction, cmds <> rest)
    | otherwise -> pure (noAction, rest)

  Spawn spawned : rest ->
    (, rest) . (\s -> noAction { spawn = s }) <$> spawned

  LoadTextBox acts rtb : rest -> do
    tb <- rtb
    pure (acts, WaitTextBox (acts { changeSprite = Nothing }) tb : rest)

  WaitTextBox acts tb : rest -> do
    TB.update input tb >>= \case
      Nothing -> pure  (acts, rest)
      Just tb' -> pure (acts, WaitTextBox acts tb' : rest)

  PlayMusic times (name, m) : rest -> do
    case m of
      Nothing ->
        error $ "Audio asset not loaded: " ++ name
      Just msc ->
        pure (noAction, PlayMusic' times msc : rest)

  PlayMusic' _ _ : rest ->
    pure (noAction, rest) -- `render` takes care of playing the music

  StopMusic : rest ->
    pure (noAction, rest) -- `render` takes care of playing the music

  Shake : rest ->
    pure (noAction { shake = True }, rest)

  FadeOut actions n : rest ->
    let
      jump = 2
    in case n of
      255 ->
        pure (actions, rest)
      _ | n > 255 - jump ->
        pure (noAction, FadeOut actions 255 : rest)
      _ ->
        pure (noAction, FadeOut actions (n + jump) : rest)

  FadeIn actions n : rest ->
    let
      jump = 2
    in case n of
      0 -> pure (actions, rest)
      _ | n < jump ->
        pure (noAction, FadeIn actions 0 : rest)
      _ ->
        pure (noAction, FadeIn actions (n - jump) : rest)

  TextUp spd font location txt : rest ->
    case location ^. y of
      n | n < -50 ->
        pure (noAction, rest)
      n ->
        pure (noAction, TextUp spd font (set y (n - fromIntegral spd) location) txt : rest)




goToLoc :: IPoint -> Command obj
goToLoc p =
  WaitUntil
    (act { moveMC = Just p })
    (\mcPos _ -> case mcPos of
        Nothing -> False
        Just posi -> isAround p posi (Point 20 20)
    )

render :: SDL.Renderer -> Size -> Camera -> Script obj -> IO ()
render renderer sz cam =
  maybe (pure ()) f . listToMaybe
  where
    f = \case
      WaitTextBox _ tb ->
        TB.render renderer tb

      PlayMusic' times m ->
        Mix.playMusic times =<< Mix.decode m

      StopMusic ->
        void $ Mix.fadeOutMusic (1000 * 2) -- milliseconds

      FadeOut _ n -> shade renderer sz n
      FadeIn  _ n -> shade renderer sz n

      TextUp _ font location txt ->
        renderText renderer font location txt

      _ -> pure ()

