{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE DataKinds          #-}
{-# LANGUAGE DeriveGeneric      #-}
{-# LANGUAGE FlexibleInstances  #-}  -- One more extension.
{-# LANGUAGE OverloadedStrings  #-}
{-# LANGUAGE StandaloneDeriving #-}  -- To derive Show
{-# LANGUAGE TypeOperators      #-}

module Run where

import qualified Play.Engine.MySDL.MySDL as MySDL
import qualified Play.Engine.MySDL.Assets as Assets
import Play.Engine.Runner
import Play.Engine
import qualified StartScreen as S
import Alpaca.NetCode
import qualified Data.Map.Strict as M
import Control.Concurrent.Async
import Control.Concurrent
import Control.DeepSeq
import Debug.Trace
import Data.IORef
import Control.Monad
import qualified SDL
import System.Environment
import System.Exit
import System.IO

run :: IO ()
run = do
  -- race_ server client
  getArgs >>= \case
    [host, port] ->
      client host port
    [host] ->
      client host "8111"
    [] ->
      clientLocal
    _ -> do
      hPutStrLn stderr "usage: deeper [HOSTNAME] [PORT]"
      exitFailure

server = runServer "8111" 60 (mempty :: M.Map Key Bool)

client host port = do
  threadDelay 1000000
  putStrLn "Go"
  runGame <- mkGame settings
  runGame loader clientSetInput (mkWorld host port) clientSample clientStop clientPlayerId

clientLocal = do
  putStrLn "Go"
  input <- newIORef (mempty :: M.Map PlayerId Input)
  let
    mkworld
      :: Assets.Resources
      -> IO (IORef ([Assets.Request], (Settings, Stack Scene)))
    mkworld assets = do
      ref <- newIORef ([], (settings, S.mkState 5 assets `Stack` []))
      forkIO $ forever $ do
        start <- SDL.ticks
        inputs <- readIORef input
        (_, world) <- readIORef ref
        let world' = update inputs world
        writeIORef ref world'
        end <- SDL.ticks
        MySDL.regulateFPS 60 start end
      pure ref
  runGame <- mkGame settings
  runGame
    loader
    (\_ -> writeIORef input . M.singleton (PlayerId 0))
    mkworld
    readIORef -- (fmap (maybe mempty id . M.lookup (PlayerId 0)) . readIORef)
    (const $ pure ())
    (pure $ PlayerId 0)

loader = Assets.runRequest $ Assets.Load S.wantedAssets

mkWorld host port assets =
  -- runClient "5.180.31.107" "8111" 60 mempty
  -- runClient "127.0.0.1" "8111" 60 mempty
  runClient host port 60 emptyInput
    ([], (settings, S.mkState 5 assets `Stack` []))
    ( \inputs _ (_, world) ->
      let
        res = update inputs world
      in
        res
    )

settings :: Settings
settings = def
  { _windowSize = Point 768 576
  , _windowTitle = "Deep"
  }
