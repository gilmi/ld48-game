
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module StartScreen where

import Data.Maybe (fromJust)
import SDL.Vect (V4(..))
import qualified SDL
import qualified Play.Engine.MySDL.MySDL as MySDL
import qualified Play.Engine.MySDL.Assets as Assets

import Control.Monad.Except
import qualified Control.Monad.State as SM
import Control.Lens
import Data.Bifunctor
import Data.Bool
import qualified Data.Map.Strict as M

import Play.Engine hiding (head)
import qualified Play.Engine.Sprite as Spr
import qualified Play.Engine.ListZipper as Z
import qualified Play.Engine.Button as Btn

import qualified GameScreen as G


data State
  = State
  { _bg :: Spr.Sprite
  , _buttons :: Z.ListZipper Identity (Btn.Button, Result StackCommand)
  , _cheat :: !Int
  , _resources :: Assets.Resources
  }

makeFieldsNoPrefix ''State

wantedAssets :: [(String, Assets.ResourceType FilePath)]
wantedAssets =
  [ ("bg", Assets.Texture "op2.png")
  , ("unispace", Assets.Font "unispace/unispace.ttf")
  ] <> G.wantedAssets

mkState :: Int -> Assets.Resources -> Scene
mkState cheat_ rs =
  let state = initState cheat_ rs
  in mkScene
    state
    update
    render

initState :: Int -> Assets.Resources -> State
initState cheat_ rs = do
  case (,)
    <$> M.lookup "bg" (Assets.textures rs)
    <*> M.lookup "unispace" (Assets.fonts rs) of
    Nothing ->
      error "Texture not found: bg or unispace"
    Just (bgt, fnt) ->
      let
        makeBtn' n =
          Btn.make (Point 120 (200 + n * 60)) (Point 180 50) fnt

        makeBtn name state n =
          (makeBtn' n name, pure $ Push state)

        btns = zipWith (flip ($)) [0..] $
          [ makeBtn "Start" (G.mkState rs)
          , \n -> (makeBtn' n "Exit", SM.modify (set quit True) *> pure None)
          ]

        st = State
          { _bg =
            fromJust
              $ Spr.make
              $ Spr.MakeArgs
              { mkActionmap = ["normal"]
              , mkAction = "normal"
              , mkTexture = bgt
              , mkSize = Point 768 576
              , mkMaxPos = 8
              , mkSpeed = 8
              }
          , _buttons = Z.ListZipper
            []
            (Identity $ head btns)
            (tail btns)
          , _cheat = cheat_
          , _resources = rs
          }

      in
        st

update :: Inputs -> Settings -> State -> Result (StackCommand, State)
update inputs _ state = do
  _wSize <- _windowSize <$> SM.get
  btns <- Z.diffMapM (firstM $ Btn.update empty) (firstM $ Btn.update inputs)
    $ if
       | any (keyClicked KeyDown) inputs ->
         Z.nextCycle (state ^. buttons)

       | any (keyClicked KeyUp) inputs ->
         Z.prevCycle (state ^. buttons)

        | otherwise ->
          state ^. buttons

  let ((check, _), cmd') = Z.get btns
  cmd <- bool (pure None) cmd' check
  pure
    ( cmd
    , state
      & set buttons (fmap (first snd) btns)
      & over cheat (if any (keyClicked KeyC) inputs then (\c -> c - 1) else id)
    )

render :: SDL.Renderer -> Settings -> State -> IO ()
render renderer settings state = do
  let n = fromIntegral $ max (-1) (state ^. cheat) * 8
  void $ MySDL.setBGColor (V4 (10 + n) 0 20 255) renderer
  Spr.render renderer id (Point 0 0) (state ^. bg . size) 255 (state ^. bg)
  -- shade renderer (settings ^. windowSize) (160 + n)
  void $ Z.diffMapM
    (Btn.render renderer False)
    (Btn.render renderer True)
    (fmap fst $ state ^. buttons)

