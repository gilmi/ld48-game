-- MySDL: some wrappers and utility functions around SDL

{-# LANGUAGE LambdaCase, TypeFamilies #-}

module Play.Engine.MySDL.Assets where

import Control.Exception (catch, SomeException(..))
import Control.Monad.Identity
import Control.Concurrent.STM
import Control.Concurrent.Async
import qualified Data.ByteString as BS

import qualified Data.Map.Strict as M

import qualified SDL
import qualified SDL.Image as SDLI
import qualified SDL.Font as SDLF
import qualified SDL.Mixer as Mix

import Play.Engine.Types (Size)
import Play.Engine.Utils (scalePoint)


data Resource
  = RTexture SDL.Texture
  | RFont SDLF.Font
  | RMusic BS.ByteString

data ResourceType a
  = Texture a
  | Font a
  | Music a

data Request
  = Load ![(String, ResourceType FilePath)]
  | DestroyTexture SDL.Texture
  | PlayMusic (String, FilePath)
  | MuteMusic
  | UnmuteMusic
  | SetNormalWindowScale Size
  | SetSmallWindowScale Size

data ResourcesT f
  = Resources
  { textures :: HKD f (M.Map FilePath SDL.Texture)
  , fonts :: HKD f (M.Map FilePath SDLF.Font)
  , music :: HKD f (M.Map FilePath BS.ByteString)
  }

type family HKD f a where
  HKD Identity a = a
  HKD f        a = f a

type Resources = ResourcesT Identity

initResources :: IO (ResourcesT TVar)
initResources =
  Resources
    <$> newTVarIO M.empty
    <*> newTVarIO M.empty
    <*> newTVarIO M.empty

runRequest :: Request -> ResourcesT TVar -> SDL.Window -> SDL.Renderer -> IO (Maybe Resources)
runRequest req resources window renderer =
  flip catch (\(SomeException e) -> error $ show e) $
    case req of
      Load files -> do
        results <-
          mapConcurrently
            (loadResource renderer resources)
            files
        pure (Just $ resourcesToResponse results)
      DestroyTexture txt -> do
        SDL.destroyTexture txt
        pure Nothing
      PlayMusic (n, p) -> do
        (_, RMusic msc) <- loadResource renderer resources (n, Music p)
        Mix.playMusic Mix.Forever =<< Mix.decode msc
        pure Nothing
      MuteMusic -> do
        Mix.setMusicVolume 0
        pure Nothing
      UnmuteMusic -> do
        Mix.setMusicVolume 100
        pure Nothing
      SetSmallWindowScale size -> do
        SDL.windowSize window SDL.$= (scalePoint 0.7 size)
        SDL.rendererScale renderer SDL.$= 0.7
        SDL.setWindowPosition window SDL.Centered
        pure Nothing
      SetNormalWindowScale size -> do
        SDL.windowSize window SDL.$= fmap fromIntegral size
        SDL.rendererScale renderer SDL.$= 1
        SDL.setWindowPosition window SDL.Centered
        pure Nothing


loadResource renderer resources (n, r) =
  case r of
    Texture (("assets/imgs/" ++) -> f) -> do
      mTxt <- atomically $ do
        txts <- readTVar (textures resources)
        pure $ M.lookup f txts
      (n,) . RTexture <$> case mTxt of
        Just txt ->
          pure txt
        Nothing -> do
          txt <- SDLI.loadTexture renderer f
          atomically $ do
            txts' <- readTVar (textures resources)
            writeTVar (textures resources) (M.insert f txt txts')
          pure txt

    Font (("assets/fonts/" ++) -> f) -> do
      mFont <- atomically $ do
        fnts <- readTVar (fonts resources)
        pure $ M.lookup f fnts
      (n,) . RFont <$> case mFont of
        Just fnt ->
          pure fnt
        Nothing -> do
          fnt <- SDLF.load f 18
          atomically $ do
            fnts' <- readTVar (fonts resources)
            writeTVar (fonts resources) (M.insert f fnt fnts')
          pure fnt

    Music (("assets/audio/" ++) -> f) -> do
      mMusic <- atomically $ do
        msc <- readTVar (music resources)
        pure $ M.lookup f msc
      (n,) . RMusic <$> case mMusic of
        Just msc ->
          pure msc
        Nothing -> do
          contents <- BS.readFile f
          atomically $ do
            msc' <- readTVar (music resources)
            writeTVar (music resources) (M.insert f contents msc')
          pure contents

resourcesToResponse :: [(String, Resource)] -> Resources
resourcesToResponse rs =
  foldr (flip g) initS $ rs
  where
    initS = Resources M.empty M.empty M.empty
    g s = \case
      (n, RTexture t) -> s { textures = M.insert n t (textures s) }
      (n, RFont f) -> s { fonts = M.insert n f (fonts s) }
      (n, RMusic m) -> s { music = M.insert n m (music s) }
