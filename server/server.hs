{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
module Main where

import Alpaca.NetCode
import GHC.Generics
import Control.DeepSeq
import Flat.Class

main = runServer "8111" 60 emptyInput

data Input
  = Input
    { keyUp    :: !Bool
    , keyDown  :: !Bool
    , keyLeft  :: !Bool
    , keyRight :: !Bool
    , keyA     :: !Bool
    , keyB     :: !Bool
    , keyC     :: !Bool
    , keyD     :: !Bool
    , keyM     :: !Bool
    , keyP     :: !Bool
    , keyScale :: !Bool
    , keyStart :: !Bool
    , keyQuit  :: !Bool
    }
  deriving (Show, Eq, Ord, Generic, NFData, Flat)

emptyInput :: Input
emptyInput = Input
  { keyUp    = False
  , keyDown  = False
  , keyLeft  = False
  , keyRight = False
  , keyA     = False
  , keyB     = False
  , keyC     = False
  , keyD     = False
  , keyM     = False
  , keyP     = False
  , keyScale = False
  , keyStart = False
  , keyQuit  = False
  }
