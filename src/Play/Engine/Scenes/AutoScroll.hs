
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE DuplicateRecordFields  #-}
{-# LANGUAGE FlexibleInstances  #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module Play.Engine.Scenes.AutoScroll where

import SDL.Vect (V4(..))
import qualified SDL
import qualified SDL.Mixer as Mix
import qualified Play.Engine.MySDL.Assets as Assets
import qualified Play.Engine.MySDL.MySDL as MySDL

import Play.Engine
import Control.Monad.Except
import Control.Lens
import System.Random
import qualified Control.Monad.State as SM

import qualified Play.Engine.Script as Script
import qualified Play.Engine.Sprite as Spr


data State
  = State
  { _bg :: Maybe Spr.Sprite
  , _script :: Script.Script ()
  , _camera :: !Int
  , _exit :: !Bool
  }

makeFieldsNoPrefix ''State

wantedAssets :: [(String, Assets.ResourceType FilePath)]
wantedAssets =
  [ ("unispace", Assets.Font "unispace/unispace.ttf")
  ]

make :: Script.Script () -> Scene
make scrpt = do
  mkScene
    (initState scrpt)
    update
    render

initState :: Script.Script () -> State
initState scrpt = do
  State
    { _bg = Nothing
    , _script = scrpt
    , _camera = 0
    , _exit = False
    }

update :: Inputs -> Settings -> State -> Result (StackCommand, State)
update input _ state = do
  _wSize <- _windowSize <$> SM.get

  (acts, script') <- Script.update input Nothing mempty (state ^. script)

  let
    newState =
      state'
        & set script script'
        & over camera
          (\c ->
             if
               | c <= 0 && Script.shake acts -> 60
               | c <= 0 -> 0
               | otherwise -> c - 1
          )
      where
        state' =
          if Script.stopTheWorld acts
            then
              state
            else
              state
                & over (bg . _Just)
                ( case Script.changeSprite acts of
                    Nothing -> Spr.update Nothing False
                    Just sp -> const sp
                )

  if
    | any (keyReleased KeyQuit) input -> do
      pure (None, set exit True state)
    | state ^. exit -> do
      pure (Done, state)
    | otherwise ->
      pure (Script.command acts, newState)

render :: SDL.Renderer -> Settings -> State -> IO ()
render renderer settings state = do
  cam' <- Point <$> randomRIO (-1, 1) <*> randomRIO (-1, 1) :: IO FPoint
  let cam = addPoint $ fmap (floor . (*) (fromIntegral $ state ^. camera `div` 3)) cam'
  void $ MySDL.setBGColor (V4 0 0 0 255) renderer
  case state ^. bg of
    Nothing -> pure ()
    Just bgSpr ->
      Spr.render renderer cam (Point 0 0) (bgSpr ^. size) 255 bgSpr

  Script.render renderer (settings ^. windowSize) cam (state ^. script)

  when (state ^. exit) $
    Mix.haltMusic
